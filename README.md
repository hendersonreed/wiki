## Important features of mine:

1. use a custom-written web server, rather than a specially configured standard one.
    - the primary responsibility would be to show the compiled pages.
    - additionally, it would expose a small and simple API to allow editing of pages using a WYSIWYG edit page.
        - this is actually hugely important ime and should be an early requirement.
2. Several options to write pages:
    - would like the same WYSIWYG page editor, if possible.
    - support any markup to html converter
         - hmmmm nope.
    - local page editing
    - optional feature: executable pages, where printed text becomes the page.
         - this is the way, everything will be built this way. Provide an entrypoint script, and then you can use any converter you want.
3. fewest possible installed dependencies:
    - I'd like to use as few as possible Racket dependencies.
    - there probably needs to be some kind of external dependency for the WYSIWYG page editor. EDIT: actually, we _might_ be able to port the one from minisleep to fit our needs. EDIT 2: there is enough in HTML and JS that we don't need this, we can build our own.
4. HTTP authentication.
    - by default, we'll just require that my password be passed with any requests to the server.
    - adding users can be done manually (probably use token auth like uelen does for Terra.)
5. easy to theme
    - single CSS file ideally.
6. pages stored in files and folders.
    - yeah, no need for a serious DB.

## On-disk storage

Each page is stored in the file tree as source and as compiled output.

There's simply two folders in a configured location (default is `/home/<user>/.config/wiki/`): `/home/<user>/.config/wiki/input` and `/home/<user>/.config/wiki/output`.

## compiling process

each page simply gets a script run against it. That script _could_ just run `scribble` on it. Or it could do any number of fancy things. At the outset, let's just use a markdown renderer to render it and give it header/footer/

## backlinks

there are a few things we could do to handle backlinks.

- a persistent link database, with javascript in the page template to query the server, which queries the db.
    - imo, this is the most flexible. It also can be added later, so we don't need to worry about it now.
    - oh my god... so we only need to generate backlinks when someone browses the page. And to actually find out if a page links to the given page is just a `grep` command. So, we could do JIT backlink generation.
